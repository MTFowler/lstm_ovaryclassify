The automatic classification of Pyriproxyfen affected mosquito ovaries

Contains the code and model weights for reproducing the results and figures from the manuscript
“The automatic classification of Pyriproxyfen affected mosquito ovaries”
MT Fowler, RS Lees, J Fagbohoun, NS Matow, C Ngufor, N Protopopoff, A Spiers